# 09 - Quantitative Image Analysis for Mechanics

Practical sessions with [Emmanuel Roubin](mailto:emmanuel.roubin@univ-grenoble-alpes.fr).

## Access the notebooks
### Launch binder to access the notebooks

- [Lunch binder on mybinder.org](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Froubine%2F09-image-analysis/HEAD?urlpath=tree)
- [Lunch binder on plmbinder.math.cnrs.fr](https://plmbinder.math.cnrs.fr/binder/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Froubine%2F09-image-analysis/HEAD?urlpath=tree)


:warning: With binder you only have a *temporary* space for your notebooks. You will have to save them on your computer if you want to use them again.

### With docker
Create the container (first time needs interactive `-it` if the token is needed)
```bash
# needs to be run inside a folder with the notebooks
docker run -it -p 8888:8888 -v "${PWD}":/home/jovyan --name 09 quay.io/jupyter/base-notebook
```

Start the container (*i.e* the jupyter notebook)
```bash
# use -a to start and attach in order to see the auth token
docker start -a 09
```

Install system dependencies and python modules in the container
```bash
# install apt from root
docker exec -u root -it 09 /bin/bash -c "apt update; apt install $(xargs < apt.txt)"
# install pip from default user
docker exec -it 09 pip install $(xargs < requirements.txt)
```

Stop the container (*i.e* the jupyter notebook)
```bash
docker stop 09
```

## Introduction and external resources (Home)
Please have a careful look at the `Introduction.ipynb` notebook **before the first practical session**. It covers the very basics of Python to handle images.

You need to be comfortable with the content of each cell in order to follow the two sessions.

I also provide you with some external resources for each session. It is **strongly advised** to look at them thoroughly before each session.

## Session 1: Filters (3h)
In this session, you will code your own generic kernel-based filter. Then, you will use it on a real image of a geomaterial to identify the different phases (matrix, inclusions, porosity).

_External Resources (Home)_
- [Wikipedia page on kernels](https://en.wikipedia.org/wiki/Kernel_(image_processing))
- [3blue1brown video on convolutions](https://www.youtube.com/watch?v=KuXjwB4LzSA&t=2s)

## Session 2: Registration (3h)
In this session, you will code your own affine operator that transforms an image. It will then be used to perform a registration in order to align two images.

_External Resources (Home)_
- [SPAM Tutorial: Image correlation – Theory](https://www.spam-project.dev/docs/tutorials/tutorial-02a-DIC-theory.html)
